import 'package:flutter/material.dart';

void main() => runApp(AnimateBall());

class AnimateBall extends StatelessWidget{

  Widget build(BuildContext context){
    return MaterialApp(
      title: 'Animate Ball',
      home: AnimateBallStateFull()
    );
  }
}

class AnimateBallStateFull extends StatefulWidget{
  @override
  _AnimateBallStateFull createState() => _AnimateBallStateFull();
}

class _AnimateBallStateFull extends State<AnimateBallStateFull> with SingleTickerProviderStateMixin {
  

  Animation<double> animation;
  AnimationController animationController;
  bool animationFlag = true;

  initState(){
    super.initState();
    animationController = new AnimationController( 
                                                    duration: const Duration(milliseconds: 300), 
                                                    vsync: this
                                                    );
    animation = Tween(begin: 50.0, end: 300.0).animate(animationController);
    animationController.forward();
  }

  


  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('Animate Ball App Bar'),
      ),
      body: _buildWidgets(),
    );
  }

  Widget _buildWidgets(){
    return new Stack(
      children: <Widget>[
        Positioned(
          left: animation.value,
          top: 150,
          child: Container(
            height: 100,
            width: 100,
            decoration: BoxDecoration(
              color: Colors.yellow,
              shape: BoxShape.circle
            ),
          )
        ),
        Positioned(
          top: 300,
          left: 200,
          child: RaisedButton(
            child: Text('Move me!'),
            onPressed: _animate,
          )        
        )

      ],
    );
  }

  void _animate(){
    // animation.addStatusListener((status) {
    //   if (status == AnimationStatus.completed) {
    //     animationController.reverse();
    //   } else if (status == AnimationStatus.dismissed) {
    //     animationController.forward();
    //   }
    // });
    setState((){
        if (animationFlag == true){
          animationController.reverse();
          
        }
        else{
          animationController.forward();
        } 
        animationFlag = !animationFlag;   
      }
    );
  }

  dispose() {
    animationController.dispose();
    super.dispose();
  }

}

